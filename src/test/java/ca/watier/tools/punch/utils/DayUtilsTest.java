/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.utils;

import ca.watier.tools.punch.models.Day;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalTime;

public class DayUtilsTest {

    @Test
    public void calculateTotalWorkingHours() {
        // given
        Day givenFirstDay = Day.now();
        givenFirstDay.addPunch(LocalTime.of(7, 30), LocalTime.of(12, 0));
        givenFirstDay.addPunch(LocalTime.of(12, 15), LocalTime.of(15, 30));

        Day givenSecondDay = Day.now();
        givenSecondDay.addPunch(LocalTime.of(7, 30), LocalTime.of(12, 0));
        givenSecondDay.addPunch(LocalTime.of(12, 15), LocalTime.of(15, 30));

        // when
        double total = DayUtils.calculateTotalRawWorkingHours(givenFirstDay, givenSecondDay);

        // then
        Assertions.assertThat(total).isEqualTo(15.50);
    }

    @Test
    public void calculateTotalWorkingHours_less_than_an_hour() {
        // given
        Day givenFirstDay = Day.now();
        givenFirstDay.addPunch(LocalTime.of(7, 30), LocalTime.of(7, 45));

        // when
        double total = DayUtils.calculateTotalRawWorkingHours(givenFirstDay);

        // then
        Assertions.assertThat(total).isEqualTo(0.25);
    }

    @Test
    public void calculateTotalRealWorkingHours_forth() {
        // given
        Day givenFirstDay = Day.now();
        givenFirstDay.addPunch(LocalTime.of(7, 30), LocalTime.of(7, 45));

        // when
        double total = DayUtils.calculateTotalRealWorkingHours(givenFirstDay);

        // then
        Assertions.assertThat(total).isEqualTo(0.15);
    }

    @Test
    public void calculateTotalRealWorkingHours_half() {
        // given
        Day givenFirstDay = Day.now();
        givenFirstDay.addPunch(LocalTime.of(7, 0), LocalTime.of(7, 30));

        // when
        double total = DayUtils.calculateTotalRealWorkingHours(givenFirstDay);

        // then
        Assertions.assertThat(total).isEqualTo(0.30);
    }

    @Test
    public void calculateTotalRealWorkingHours_third() {
        // given
        Day givenFirstDay = Day.now();
        givenFirstDay.addPunch(LocalTime.of(7, 0), LocalTime.of(7, 45));

        // when
        double total = DayUtils.calculateTotalRealWorkingHours(givenFirstDay);

        // then
        Assertions.assertThat(total).isEqualTo(0.45);
    }

    @Test
    public void calculateTotalRealWorkingHours_full() {
        // given
        Day givenFirstDay = Day.now();
        givenFirstDay.addPunch(LocalTime.of(7, 0), LocalTime.of(8, 0));

        // when
        double total = DayUtils.calculateTotalRealWorkingHours(givenFirstDay);

        // then
        Assertions.assertThat(total).isEqualTo(1.0);
    }

}