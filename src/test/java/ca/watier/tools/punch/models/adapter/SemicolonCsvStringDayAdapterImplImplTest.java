/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.models.adapter;

import ca.watier.tools.punch.adapters.DayAdapter;
import ca.watier.tools.punch.adapters.SemicolonCsvStringDayAdapterImpl;
import ca.watier.tools.punch.models.Day;
import ca.watier.tools.punch.models.Punch;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SemicolonCsvStringDayAdapterImplImplTest {

    @Test
    public void from_estimation() {
        // given
        LocalDate givenDate = LocalDate.of(2020, 6, 16);
        LocalTime givenMorningStartTime = LocalTime.of(7, 30);
        LocalTime givenMorningEndTime = LocalTime.of(12, 0);
        LocalTime givenAfterNoonStartTime = LocalTime.of(12, 15);
        LocalTime givenAfterNoonEndTime = LocalTime.of(15, 30);

        String givenDay = "16/06/2020;730;1200;1215;1530";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        Day from = dayAdapter.convert();

        // then

        List<Punch> punches = from.getPunches();
        Punch morningPunch = punches.get(0);
        Punch afterNoon = punches.get(1);

        assertThat(from.getDate()).isEqualTo(givenDate);
        assertThat(morningPunch.getStartTime()).isEqualTo(givenMorningStartTime);
        assertThat(morningPunch.getEndTime()).isEqualTo(givenMorningEndTime);
        assertThat(afterNoon.getStartTime()).isEqualTo(givenAfterNoonStartTime);
        assertThat(afterNoon.getEndTime()).isEqualTo(givenAfterNoonEndTime);
    }

    @Test(expected = IllegalStateException.class)
    public void from_empty() {
        String givenDay = ";;;;";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        dayAdapter.convert();
    }

    @Test
    public void from_date_only() {
        // given
        String givenDay = "16/06/2020;;;;";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        Day convert = dayAdapter.convert();

        // then
        assertThat(convert.getPunches()).isEmpty();

    }

    @Test
    public void from_date_status_only() {
        // given
        String givenDay = "16/06/2020;;;;";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        Day convert = dayAdapter.convert();

        // then
        assertThat(convert.getPunches()).isEmpty();
    }

    @Test
    public void from_date_status_morning_start_only() {
        // given
        String givenDay = "16/06/2020;730;;;";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        Day convert = dayAdapter.convert();

        // then
        assertThat(convert.getPunches()).isEmpty();
    }

    @Test
    public void from_date_status_morning_only() {
        // given
        String givenDay = "16/06/2020;730;1200;;";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        Day convert = dayAdapter.convert();

        // then
        assertThat(convert.getDate()).isEqualTo(LocalDate.of(2020, 6, 16));

        List<Punch> punches = convert.getPunches();
        assertThat(punches).hasSize(1);

        Punch punch = punches.get(0);
        assertThat(punch.getStartTime()).isEqualTo(LocalTime.of(7, 30));
        assertThat(punch.getEndTime()).isEqualTo(LocalTime.of(12, 0));
    }

    @Test
    public void from_date_status_morning_and_start_noon_only() {
        // given
        String givenDay = "16/06/2020;730;1200;1215;";

        // when
        DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(givenDay);
        Day convert = dayAdapter.convert();

        // then
        assertThat(convert.getDate()).isEqualTo(LocalDate.of(2020, 6, 16));

        List<Punch> punches = convert.getPunches();
        assertThat(punches).hasSize(1);

        Punch punch = punches.get(0);
        assertThat(punch.getStartTime()).isEqualTo(LocalTime.of(7, 30));
        assertThat(punch.getEndTime()).isEqualTo(LocalTime.of(12, 0));
    }
}