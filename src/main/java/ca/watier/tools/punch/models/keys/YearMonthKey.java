/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.models.keys;

import java.time.LocalDate;
import java.util.Objects;

public class YearMonthKey extends YearKey {
    private static final long serialVersionUID = -2462764104159571845L;

    private final int monthAsInt;

    public YearMonthKey(int year, int monthAsInt) {
        super(year);
        this.monthAsInt = monthAsInt;
    }

    public static YearMonthKey from(LocalDate date) {
        if (date == null) {
            return null;
        }

        int year = date.getYear();
        int monthAsInt = date.getMonthValue();

        return from(year, monthAsInt);
    }

    public static YearMonthKey from(int year, int monthAsInt) {
        return new YearMonthKey(year, monthAsInt);
    }

    public static YearMonthKey now() {
        LocalDate date = LocalDate.now();

        int year = date.getYear();
        int monthAsInt = date.getMonthValue();

        return from(year, monthAsInt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), monthAsInt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        YearMonthKey that = (YearMonthKey) o;
        return monthAsInt == that.monthAsInt;
    }

    @Override
    public String toString() {
        return "YearMonthKey{" +
                "monthAsInt=" + monthAsInt +
                '}';
    }

    public int getMonthAsInt() {
        return monthAsInt;
    }
}
