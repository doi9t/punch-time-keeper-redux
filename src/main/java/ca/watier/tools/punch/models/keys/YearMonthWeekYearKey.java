/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.models.keys;

import ca.watier.tools.punch.customizers.LanguageCustomizers;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Objects;

public class YearMonthWeekYearKey extends YearMonthKey {
    private static final long serialVersionUID = 4824340166190871970L;

    private final int weekOfYear;

    public YearMonthWeekYearKey(int year, int monthAsInt, int weekOfYear) {
        super(year, monthAsInt);
        this.weekOfYear = weekOfYear;
    }

    public static YearMonthWeekYearKey from(LocalDate date) {
        if (date == null) {
            return null;
        }

        int year = date.getYear();
        int monthAsInt = date.getMonthValue();
        int weekOfYear = date.get(WeekFields.of(LanguageCustomizers.getLocale()).weekOfYear());

        return from(year, monthAsInt, weekOfYear);
    }

    public static YearMonthWeekYearKey from(int year, int monthAsInt, int weekOfYear) {
        return new YearMonthWeekYearKey(year, monthAsInt, weekOfYear);
    }

    public static YearMonthWeekYearKey now() {
        LocalDate date = LocalDate.now();

        int year = date.getYear();
        int monthAsInt = date.getMonthValue();
        int weekOfYear = date.get(WeekFields.of(LanguageCustomizers.getLocale()).weekOfYear());

        return from(year, monthAsInt, weekOfYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weekOfYear);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        YearMonthWeekYearKey that = (YearMonthWeekYearKey) o;
        return weekOfYear == that.weekOfYear;
    }

    public int getWeekOfYear() {
        return weekOfYear;
    }
}
