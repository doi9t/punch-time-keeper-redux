/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.models;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

public class Punch implements Serializable {
    private static final long serialVersionUID = 5787932167986576668L;

    private final LocalTime startTime;
    private final LocalTime endTime;

    private Punch(LocalTime startTime, LocalTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static Punch of(LocalTime startTime, LocalTime endTime) {
        if (startTime == null || endTime == null) {
            throw new IllegalArgumentException("The start or end time cannot be empty!");
        } else if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("The start date cannot be after the end date!");
        }

        return new Punch(startTime, endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Punch punch = (Punch) o;
        return Objects.equals(startTime, punch.startTime) &&
                Objects.equals(endTime, punch.endTime);
    }

    @Override
    public String toString() {
        return "Punch{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public LocalTime getStartTime() {
        return startTime;
    }
}
