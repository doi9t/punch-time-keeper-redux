/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.tools.punch.models;

import org.apache.commons.lang3.StringUtils;

public enum OptionWithProperties {
    HELP(
            "h",
            "help",
            false,
            "Print this help.",
            0,
            null
    ),
    FILE(
            "f",
            "file",
            true,
            "The file with the path to be used.",
            1,
            "file"
    ),
    LANG(
            "l",
            "lang",
            true,
            "The language to be used (ISO 639).",
            1,
            "language (ISO 639)"
    ),
    SHOW_CURRENT_WEEK(
            "w",
            "week",
            false,
            "Show the total hours for the current week.",
            0,
            null
    ),
    SHOW_LAST_X_WEEKS(
            "xw",
            "xweek",
            true,
            "Show the total hours for each last x week(s).",
            1,
            "number of x week(s)"
    ),
    SHOW_CURRENT_MONTH(
            "m",
            "month",
            false,
            "Show the total hours for the current month.",
            0,
            null
    ),
    SHOW_LAST_X_MONTH(
            "xm",
            "xmonth",
            true,
            "Show the total hours for each last x month(s)",
            1,
            "number of x month(s)"
    ),
    SHOW_CURRENT_YEAR(
            "y",
            "year",
            false,
            "Show the total hours for the current year.",
            0,
            null
    ),
    SHOW_LAST_X_YEARS(
            "xy",
            "xyear",
            true,
            "Show the total hours for each last x year(s)",
            1,
            "number of x year(s)"
    ),
    SHOW_RANGE(
            "r",
            "range",
            true,
            "Show the total hours for the range of date (d/MM/yyyy)",
            2,
            "date (d/MM/yyyy)> <date (d/MM/yyyy)"
    );

    private final String shortOption;
    private final String longOption;
    private final boolean hasArg;
    private final String description;
    private final int nbOfArgs;
    private final String argName;

    OptionWithProperties(String shortOption, String longOption, boolean hasArg, String description, int nbOfArgs, String argName) {
        this.shortOption = shortOption;
        this.longOption = longOption;
        this.hasArg = hasArg;
        this.description = description;
        this.nbOfArgs = nbOfArgs;
        this.argName = argName;
    }

    public String getArgName() {
        return argName;
    }

    public String getDescription() {
        return description;
    }

    public String getFirstNonNullOption() {
        return StringUtils.firstNonBlank(shortOption, longOption);
    }

    public String getLongOption() {
        return longOption;
    }

    public int getNbOfArgs() {
        return nbOfArgs;
    }

    public String getShortOption() {
        return shortOption;
    }

    public boolean isHasArg() {
        return hasArg;
    }
}
