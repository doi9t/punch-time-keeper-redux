/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.models.keys;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class YearKey implements Serializable {
    private static final long serialVersionUID = -2462764104159571845L;

    private final int year;

    public YearKey(int year) {
        this.year = year;
    }

    public static YearKey now() {
        return from(LocalDate.now());
    }

    public static YearKey from(LocalDate date) {
        if (date == null) {
            return null;
        }
        return from(date.getYear());
    }

    public static YearKey from(int year) {
        return new YearKey(year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YearKey yearKey = (YearKey) o;
        return year == yearKey.year;
    }

    @Override
    public String toString() {
        return "YearKey{" +
                "year=" + year +
                '}';
    }

    public int getYear() {
        return year;
    }
}
