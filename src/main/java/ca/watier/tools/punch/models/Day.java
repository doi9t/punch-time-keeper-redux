/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.models;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class Day implements Serializable {
    private static final long serialVersionUID = 4701389591886106856L;
    private final LocalDate date;
    private final List<Punch> punches = new ArrayList<>();

    private Day(LocalDate date, List<Punch> punches) {
        this(date);

        if (CollectionUtils.isEmpty(punches)) {
            return;
        }

        this.punches.addAll(punches);
    }

    public Day(LocalDate date) {
        this.date = date;
    }

    public static Day of(LocalDate date, List<Punch> punches) {
        if (date == null) {
            throw new IllegalArgumentException("The date cannot be empty!");
        }

        return new Day(date, punches);
    }

    public static Day now() {
        return new Day(LocalDate.now());
    }

    public static LocalDate getDateFromDay(Day day) {
        return Optional.ofNullable(day)
                .map(Day::getDate)
                .orElse(null);
    }

    public LocalDate getDate() {
        return date;
    }

    public void addPunches(Punch... punches) {
        if (ArrayUtils.isEmpty(punches)) {
            return;
        }

        addPunches(Arrays.asList(punches));
    }

    public void addPunches(List<Punch> punches) {
        if (CollectionUtils.isEmpty(punches)) {
            return;
        }


        this.punches.addAll(punches);
    }

    public void addPunch(LocalTime start, LocalTime end) {
        this.punches.add(Punch.of(start, end));
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, punches);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Day day = (Day) o;
        return Objects.equals(date, day.date) &&
                Objects.equals(punches, day.punches);
    }

    @Override
    public String toString() {
        return "Day{" +
                "date=" + date +
                ", punches=" + punches +
                '}';
    }

    public List<Punch> getPunches() {
        return Collections.unmodifiableList(punches);
    }
}
