/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch;

import ca.watier.tools.punch.adapters.DayAdapter;
import ca.watier.tools.punch.adapters.SemicolonCsvStringDayAdapterImpl;
import ca.watier.tools.punch.components.DayGrouping;
import ca.watier.tools.punch.components.YearKeyGroupingImpl;
import ca.watier.tools.punch.components.YearMonthKeyGroupingImpl;
import ca.watier.tools.punch.components.YearMonthWeekYearKeyGroupingImpl;
import ca.watier.tools.punch.customizers.LanguageCustomizers;
import ca.watier.tools.punch.models.Day;
import ca.watier.tools.punch.models.OptionWithProperties;
import ca.watier.tools.punch.models.keys.YearKey;
import ca.watier.tools.punch.models.keys.YearMonthKey;
import ca.watier.tools.punch.models.keys.YearMonthWeekYearKey;
import ca.watier.tools.punch.utils.DayFileUtils;
import ca.watier.tools.punch.utils.DayUtils;
import org.apache.commons.cli.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.util.*;

import static ca.watier.tools.punch.models.OptionWithProperties.*;

public class App {
    public static final HelpFormatter HELP_FORMATTER;
    private static final DateTimeFormatter DD_MM_YYYY_DATE_FORMATTER = DateTimeFormatter.ofPattern("d/MM/yyyy");
    private static final String FILE_NAME = "time.csv";

    static {
        HELP_FORMATTER = new HelpFormatter();
        HELP_FORMATTER.setWidth(480);
    }

    public static void main(String[] args) {
        CommandLine cmd = getCommandLine(args);

        if (hasOption(cmd, HELP)) {
            printHelp(getOptions());
            System.out.println("No other actions executed [...]");
            return;
        }

        File timeFile = readAndValidateFile(cmd);

        if (hasOption(cmd, LANG)) {
            changeLocale(cmd);
        }

        if (hasOption(cmd, SHOW_RANGE)) {
            printRange(cmd, timeFile);
        }

        if (hasNoOption(cmd) || hasOption(cmd, SHOW_CURRENT_WEEK)) {
            DayGrouping<YearMonthWeekYearKey> dayGrouping = new YearMonthWeekYearKeyGroupingImpl();
            printCurrentWeek(dayGrouping.readFileAndGroup(timeFile));
        }

        if (hasOption(cmd, SHOW_LAST_X_WEEKS)) {
            DayGrouping<YearMonthWeekYearKey> dayGrouping = new YearMonthWeekYearKeyGroupingImpl();
            printLastWeeks(cmd, dayGrouping.readFileAndGroup(timeFile));
        }

        if (hasOption(cmd, SHOW_CURRENT_MONTH)) {
            DayGrouping<YearMonthKey> dayGrouping = new YearMonthKeyGroupingImpl();
            printCurrentMonth(dayGrouping.readFileAndGroup(timeFile));
        }

        if (hasOption(cmd, SHOW_LAST_X_MONTH)) {
            DayGrouping<YearMonthKey> dayGrouping = new YearMonthKeyGroupingImpl();
            printLastMonths(cmd, dayGrouping.readFileAndGroup(timeFile));
        }

        if (hasOption(cmd, SHOW_CURRENT_YEAR)) {
            DayGrouping<YearKey> dayGrouping = new YearKeyGroupingImpl();
            printCurrentYear(dayGrouping.readFileAndGroup(timeFile));
        }

        if (hasOption(cmd, SHOW_LAST_X_YEARS)) {
            DayGrouping<YearKey> dayGrouping = new YearKeyGroupingImpl();
            printLastYears(cmd, dayGrouping.readFileAndGroup(timeFile));
        }
    }

    private static void printRange(CommandLine cmd, File timeFile) {
        List<LocalDate> datesParameter = getDatesParameter(2, cmd, SHOW_RANGE);
        LocalDate from = datesParameter.get(0);
        LocalDate to = datesParameter.get(1);

        List<String> rawDays = DayFileUtils.readDayFilePerLine(timeFile);
        List<Day> daysInRange = new ArrayList<>();

        for (String rawDay : rawDays) {
            DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(rawDay);
            Day converted = dayAdapter.convert();
            LocalDate date = converted.getDate();

            if (date != null && ((date.isAfter(from) && date.isBefore(to)) || date.isEqual(from) || date.isEqual(to))) {
                daysInRange.add(converted);
            }
        }

        System.out.printf("[Current range (%s - %s)]%n", from, to);
        printSummaryInHourForSpecifiedDays(daysInRange);
    }

    private static void changeLocale(CommandLine cmd) {
        String localeAsString = getStringParameter(cmd, LANG);

        try {
            Locale locale = LocaleUtils.toLocale(localeAsString);

            if (locale != null) {
                LanguageCustomizers.setLocale(locale);
            }

        } catch (IllegalArgumentException exception) {
            System.err.printf("The specified [%s] locale is invalid!", localeAsString);
        }
    }

    private static void printLastYears(CommandLine cmd, Map<YearKey, Set<Day>> readFileAndGroup) {
        int nbOfYears = getIntegerParameter(cmd, SHOW_LAST_X_YEARS);
        LocalDate currentTimeToCheck = LocalDate.now();
        int currentYear = currentTimeToCheck.getYear();

        for (int i = 0; i < nbOfYears; i++) {
            LocalDate minusOneMonth = currentTimeToCheck.minusYears(i);

            int year = minusOneMonth.getYear();

            Set<Day> daysForTheYear = readFileAndGroup.get(YearKey.from(minusOneMonth));

            if (currentYear == year) {
                System.out.printf("[%d (Current)]%n", year);
            } else {
                System.out.printf("[%d]%n", year);
            }

            printSummaryInHourForSpecifiedDays(daysForTheYear);
        }
    }

    private static void printCurrentYear(Map<YearKey, Set<Day>> readFileAndGroup) {
        Set<Day> years = readFileAndGroup.get(YearKey.now());

        System.out.println("[Current Year]");
        printSummaryInHourForSpecifiedDays(years);
    }

    private static CommandLine getCommandLine(String[] args) {
        Options options = getOptions();
        try {
            CommandLineParser parser = new DefaultParser();
            return parser.parse(options, args);
        } catch (ParseException e) {
            printHelp(options);
            System.exit(0);
            throw new IllegalStateException();
        }
    }

    private static boolean hasOption(CommandLine cmd, OptionWithProperties optionWithProperties) {
        return cmd.hasOption(optionWithProperties.getFirstNonNullOption());
    }

    private static void printHelp(Options options) {
        HELP_FORMATTER.printHelp("punch", options);
    }

    private static File readAndValidateFile(CommandLine cmd) {
        File timeFile = getTimeFile(cmd);

        if (!timeFile.exists()) {
            throw new IllegalStateException("Unable to find the file!");
        }

        return timeFile;
    }

    private static boolean hasNoOption(CommandLine cmd) {
        return ArrayUtils.isEmpty(cmd.getOptions());
    }

    private static void printCurrentWeek(Map<YearMonthWeekYearKey, Set<Day>> yearMonthWeekYear) {
        YearMonthWeekYearKey nowKey = YearMonthWeekYearKey.now();
        Set<Day> days = yearMonthWeekYear.get(nowKey);

        if (CollectionUtils.isNotEmpty(days)) {
            System.out.println("=== Current week ===");

            for (Day day : days) {
                double realHours = DayUtils.calculateTotalRealWorkingHours(day);

                LocalDate dateFromDay = Day.getDateFromDay(day);

                if (dateFromDay == null) {
                    continue;
                }

                DayOfWeek dayOfWeek = dateFromDay.getDayOfWeek();
                String displayName = dayOfWeek.getDisplayName(TextStyle.FULL, LanguageCustomizers.getLocale());

                System.out.printf("  - %s : %.2f  %n", displayName, realHours);
            }
        } else {
            System.out.println("No days found in the current week!");
        }
    }

    private static void printLastWeeks(CommandLine cmd, Map<YearMonthWeekYearKey, Set<Day>> yearMonthWeekYear) {
        int nbOfWeeks = getIntegerParameter(cmd, SHOW_LAST_X_WEEKS);
        LocalDate currentTimeToCheck = LocalDate.now();

        for (int i = 0; i < nbOfWeeks; i++) {
            LocalDate minusOneWeek = currentTimeToCheck.minusWeeks(i);

            YearMonthWeekYearKey minusKey = YearMonthWeekYearKey.from(minusOneWeek);
            Set<Day> days = yearMonthWeekYear.get(minusKey);

            if (i == 0) {
                System.out.println("[Current week]");
            } else {
                System.out.printf("[%d week ago]%n", i);
            }

            printSummaryInHourForSpecifiedDays(days);
        }
    }

    private static File getTimeFile(CommandLine cmd) {
        if (hasOption(cmd, FILE)) {
            String pathAsString = cmd.getOptionValue(FILE.getFirstNonNullOption());
            Path path = Paths.get(pathAsString);

            if (Files.isDirectory(path)) {
                return new File(path.toFile(), FILE_NAME);
            } else {
                return path.toFile();
            }
        } else {
            URL currentJar = App.class.getResource("/locator");

            File parentFolder = new File(currentJar.getPath()).getParentFile();
            if ("jar".equals(currentJar.getProtocol())) {
                parentFolder = parentFolder.getParentFile();
            }

            return new File(StringUtils.remove(parentFolder.toString(), "file:"), FILE_NAME);
        }
    }

    private static int getIntegerParameter(CommandLine cmd, OptionWithProperties option) {
        try {
            return Integer.parseInt(getStringParameter(cmd, option));
        } catch (NumberFormatException e) {
            throw NewNumberIllegalArgumentException(option);
        }
    }

    private static List<LocalDate> getDatesParameter(int nb, CommandLine cmd, OptionWithProperties option) {
        try {
            String[] stringArrayParameter = getStringArrayParameter(cmd, option);

            if (ArrayUtils.getLength(stringArrayParameter) != nb || nb < 1) {
                throw NewDateIllegalArgumentException(option);
            }

            List<LocalDate> dates = new ArrayList<>(nb);

            for (String dateParameter : stringArrayParameter) {
                dates.add(LocalDate.parse(dateParameter, DD_MM_YYYY_DATE_FORMATTER));
            }

            return dates;
        } catch (DateTimeParseException e) {
            throw NewDateIllegalArgumentException(option);
        }
    }

    private static String getStringParameter(CommandLine cmd, OptionWithProperties option) {
        return cmd.getOptionValue(option.getFirstNonNullOption());
    }

    private static String[] getStringArrayParameter(CommandLine cmd, OptionWithProperties option) {
        return cmd.getOptionValues(option.getFirstNonNullOption());
    }

    private static void printSummaryInHourForSpecifiedDays(Collection<Day> days) {
        if (CollectionUtils.isNotEmpty(days)) {
            double realHours = DayUtils.calculateTotalRealWorkingHours(days.toArray(new Day[0]));
            System.out.printf("  - %.2f hours  %n", realHours);
        } else {
            System.out.println("  - No data!");
        }
    }

    private static IllegalArgumentException NewNumberIllegalArgumentException(OptionWithProperties option) {
        return new IllegalArgumentException(String.format("Invalid parameter for the %s option! The parameters need to be an Integer.", option.getLongOption()));
    }

    private static IllegalArgumentException NewDateIllegalArgumentException(OptionWithProperties option) {
        return new IllegalArgumentException(String.format("Invalid parameter for the %s option! The parameters need to be a date (d/MM/yyyy).", option.getLongOption()));
    }

    private static void printCurrentMonth(Map<YearMonthKey, Set<Day>> yearMonthWeekYear) {
        Set<Day> days = yearMonthWeekYear.get(YearMonthKey.now());

        System.out.println("[Current month]");
        printSummaryInHourForSpecifiedDays(days);
    }

    private static void printLastMonths(CommandLine cmd, Map<YearMonthKey, Set<Day>> yearMonth) {
        int nbOfMonths = getIntegerParameter(cmd, SHOW_LAST_X_MONTH);

        LocalDate currentTimeToCheck = LocalDate.now();
        Map<YearMonthKey, Set<Day>> yearMonths = new HashMap<>();

        for (int i = 0; i < nbOfMonths; i++) {
            LocalDate minusOneMonth = currentTimeToCheck.minusMonths(i);

            YearMonthKey currentKey = YearMonthKey.from(minusOneMonth);
            yearMonths.put(currentKey, yearMonth.get(currentKey));
        }

        Month month = currentTimeToCheck.getMonth();
        int currentYear = currentTimeToCheck.getYear();
        int currentMonthAsInt = month.getValue();

        for (Map.Entry<YearMonthKey, Set<Day>> yearMonthKeySetEntry : yearMonths.entrySet()) {
            YearMonthKey key = yearMonthKeySetEntry.getKey();
            int year = key.getYear();
            int monthAsInt = key.getMonthAsInt();
            String monthNameAsString = Month.of(monthAsInt).name();

            if (currentYear == year && currentMonthAsInt == monthAsInt) {
                System.out.printf("[%s %d (Current)]%n", monthNameAsString, year);
            } else {
                System.out.printf("[%s %d]%n", monthNameAsString, year);
            }

            Set<Day> daysForTheMonth = yearMonthKeySetEntry.getValue();
            printSummaryInHourForSpecifiedDays(daysForTheMonth);
        }
    }

    private static Options getOptions() {
        Options options = new Options();

        for (OptionWithProperties optionWithProperties : OptionWithProperties.values()) {
            String longOption = optionWithProperties.getLongOption();
            String shortOption = optionWithProperties.getShortOption();
            boolean hasArg = optionWithProperties.isHasArg();
            String description = optionWithProperties.getDescription();

            Option currentOption = new Option(shortOption, longOption, hasArg, description);
            currentOption.setArgs(optionWithProperties.getNbOfArgs());
            currentOption.setArgName(optionWithProperties.getArgName());

            options.addOption(currentOption);
        }

        return options;
    }
}
