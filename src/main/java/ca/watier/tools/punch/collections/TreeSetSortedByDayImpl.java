/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.collections;

import ca.watier.tools.punch.models.Day;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetSortedByDayImpl extends TreeSet<Day> {
    private static final long serialVersionUID = 7664797936123627020L;

    public TreeSetSortedByDayImpl() {
        super(new Comparator<Day>() {
            @Override
            public int compare(Day first, Day second) {
                LocalDate firstDate = Day.getDateFromDay(first);
                LocalDate secondDate = Day.getDateFromDay(second);

                if (firstDate == null || secondDate == null) {
                    return -1;
                }

                int firstDayOfMonth = firstDate.getDayOfMonth();
                int secondDayOfMonth = secondDate.getDayOfMonth();

                return Integer.compare(firstDayOfMonth, secondDayOfMonth);
            }
        });
    }
}
