/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.utils;

import ca.watier.tools.punch.models.Day;
import ca.watier.tools.punch.models.Punch;
import org.apache.commons.lang3.ArrayUtils;

import java.time.LocalTime;

import static java.time.temporal.ChronoUnit.MINUTES;

public final class DayUtils {
    private DayUtils() {
    }

    public static double calculateTotalRealWorkingHours(Day... days) {
        double totalOfRawHours = calculateTotalRawWorkingHours(days);
        int nbOfHours = (int) totalOfRawHours;
        double minutes = totalOfRawHours - nbOfHours;

        if (nbOfHours == 0 && Double.compare(minutes, 60d) == 0) {
            return 1d;
        }

        return nbOfHours + ((minutes * 60) / 100);
    }

    public static double calculateTotalRawWorkingHours(Day... days) {
        long minutes = calculateTotalRawWorkingMinutes(days);

        if (minutes == 0L) {
            return 0d;
        }

        return minutes / 60.0;
    }

    private static long calculateTotalRawWorkingMinutes(Day[] days) {
        if (ArrayUtils.isEmpty(days)) {
            return 0L;
        }

        long nbOfMinutes = 0L;

        for (Day day : days) {
            if (day == null) {
                continue;
            }

            for (Punch punch : day.getPunches()) {
                nbOfMinutes += getNbOfMinutes(punch.getStartTime(), punch.getEndTime());
            }
        }

        return nbOfMinutes;
    }

    private static long getNbOfMinutes(LocalTime start, LocalTime end) {
        if (start == null || end == null) {
            return 0L;
        }

        return Math.abs(MINUTES.between(start, end));
    }
}
