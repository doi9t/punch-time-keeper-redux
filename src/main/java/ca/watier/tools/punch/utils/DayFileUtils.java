/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public final class DayFileUtils {

    private DayFileUtils() {
    }

    public static List<String> readDayFilePerLine(File timeFile) {
        try {
            List<String> rawDays = FileUtils.readLines(timeFile, StandardCharsets.UTF_8);

            if (rawDays.isEmpty()) {
                throw new IllegalStateException("The file is empty!");
            }

            return rawDays;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read the file!");
        }
    }
}
