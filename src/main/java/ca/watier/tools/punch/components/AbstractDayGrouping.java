/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.components;

import ca.watier.tools.punch.adapters.DayAdapter;
import ca.watier.tools.punch.adapters.SemicolonCsvStringDayAdapterImpl;
import ca.watier.tools.punch.collections.TreeSetSortedByDayImpl;
import ca.watier.tools.punch.models.Day;
import ca.watier.tools.punch.models.keys.YearKey;
import ca.watier.tools.punch.utils.DayFileUtils;

import java.io.File;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractDayGrouping<T extends YearKey> implements DayGrouping<T> {

    @Override
    public Map<T, Set<Day>> readFileAndGroup(File timeFile) {
        List<String> rawDays = DayFileUtils.readDayFilePerLine(timeFile);

        Map<T, Set<Day>> dayGroupedByYears = new HashMap<>();
        for (String rawDay : rawDays) {
            DayAdapter dayAdapter = new SemicolonCsvStringDayAdapterImpl(rawDay);
            Day day = dayAdapter.convert();
            dayGroupedByYears.computeIfAbsent(buildKeyFrom(day.getDate()), k -> new TreeSetSortedByDayImpl()).add(day);
        }

        return dayGroupedByYears;
    }

    protected abstract T buildKeyFrom(LocalDate localDate);
}
