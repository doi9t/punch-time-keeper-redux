/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.adapters;

import ca.watier.tools.punch.models.Punch;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PunchesStringAdapterImpl implements PunchesAdapter {
    private final DateTimeFormatter HH_MM_TIME_FORMATTER = DateTimeFormatter.ofPattern("Hmm");

    private final List<String> punchesAsString = new ArrayList<>();

    public PunchesStringAdapterImpl(List<String> punchesAsString) {
        if (CollectionUtils.isEmpty(punchesAsString)) {
            return;
        }

        for (String current : punchesAsString) {
            if (StringUtils.isBlank(current)) {
                continue;
            }

            this.punchesAsString.add(current);
        }
    }

    @Override
    public List<Punch> convert() {
        List<Punch> punches = new ArrayList<>(punchesAsString.size());

        for (List<String> punch : ListUtils.partition(punchesAsString, 2)) {
            if (CollectionUtils.size(punch) != 2) {
                continue;
            }

            String start = punch.get(0);
            String end = punch.get(1);

            punches.add(Punch.of(convertStringToDate(start), convertStringToDate(end)));
        }

        return Collections.unmodifiableList(punches);
    }


    protected LocalTime convertStringToDate(String punch) {

        if (StringUtils.equals(punch, "0")) {
            return LocalTime.MIDNIGHT;
        }

        String punchToBeParsed;

        try {
            switch (StringUtils.length(punch)) {
                case 1: // minutes of midnight < 10
                    punchToBeParsed = String.format("000%s", punch);
                    break;
                case 2: //  minutes of midnight >= 10
                    punchToBeParsed = String.format("00%s", punch);
                    break;
                default:
                    punchToBeParsed = punch;
                    break;
            }

            return LocalTime.parse(punchToBeParsed, HH_MM_TIME_FORMATTER);
        } catch (DateTimeParseException exception) {
            throw new IllegalStateException(String.format("The date is invalid [%s] (format=Hmm)!", punch));
        }
    }
}
