/*
 * "punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.
 *     Copyright (C) 2020  Yannick Watier
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.watier.tools.punch.adapters;

import ca.watier.tools.punch.models.Day;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public abstract class AbstractCsvStringDayAdapter implements DayAdapter {

    private static final DateTimeFormatter DD_MM_YYYY_DATE_FORMATTER = DateTimeFormatter.ofPattern("d/MM/yyyy");

    private final String day;

    public AbstractCsvStringDayAdapter(String day) {
        this.day = day;
    }

    @Override
    public Day convert() {
        if (day == null || day.isEmpty()) {
            return Day.now();
        }

        List<String> values = new ArrayList<>();
        Queue<Character> characterQueue = new ArrayDeque<>(Arrays.asList(ArrayUtils.toObject(day.toCharArray())));
        StringBuilder stringBuilder = new StringBuilder(characterQueue.size());
        while (true) {
            Character current = characterQueue.poll();
            Character next = characterQueue.poll();
            char separator = csvColumnSeparator();

            boolean isNextSeparator = next != null && separator == next;
            boolean isCurrentSeparator = current != null && separator == current;

            if (next == null) {
                if (stringBuilder.length() > 0) {
                    if (current != null && !isCurrentSeparator) {
                        stringBuilder.append(current);
                    }
                    values.add(stringBuilder.toString());
                }

                break;
            }

            if (isCurrentSeparator && stringBuilder.length() > 0) {
                values.add(stringBuilder.toString());
                stringBuilder.setLength(0);
            }

            if (isCurrentSeparator && isNextSeparator) { // Both separators
                values.add("");
                stringBuilder.setLength(0);
            } else if (!isCurrentSeparator && !isNextSeparator) { // No separators
                stringBuilder.append(current).append(next);
            } else if (!isCurrentSeparator) { // Current is not, Next is separator
                stringBuilder.append(current);
                values.add(stringBuilder.toString());
                stringBuilder.setLength(0);
            } else { // Current is separator, Next is not
                stringBuilder.append(next);
            }
        }

        LocalDate date = parseDateFromPosition(values, 0);
        PunchesAdapter punchesAdapter = new PunchesStringAdapterImpl(values.subList(1, values.size()));

        return Day.of(date, punchesAdapter.convert());
    }

    public abstract char csvColumnSeparator();

    private LocalDate parseDateFromPosition(List<String> values, int position) {
        try {
            String currentDate = StringUtils.defaultIfEmpty(values.get(position), "null");
            try {
                return LocalDate.parse(currentDate, DD_MM_YYYY_DATE_FORMATTER);
            } catch (DateTimeParseException exception) {
                throw new IllegalStateException(String.format("The current date is invalid %s (format=dd/MM/yyyy)!", currentDate));
            }
        } catch (IndexOutOfBoundsException ex) {
            throw new IllegalStateException(String.format("Unable to find the date at the position %d!", position));
        }
    }
}
