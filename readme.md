# punch-time-keeper-redux
"punch-time-keeper-redux" is a tool that helps to calculate the total of hours the person does per week; similar to a physical punch clock.

This tool DOES not write in the file (read only), you need to manually create the file and enter the time inside.

For the moment, the labels are in english only; but the days / months can be shown in the user language by specifying the `lang` parameter.

## Parameters
* Print the help (`h` or `help`)
* Specify the file location (`f` or `file`); by default if the file is `time.csv` and at the same level than the jar, it'll be read automatically.
* Set the locale (ISO 639) for the application (`l` or `lang`)

### List the hours
List the hours per single:
* Week (per days) (`w` or `week`)
* Month (`m` or `month`)
* Year (`y` or `year`)

List the hours per multiples:
* Week and the (x - 1) previous weeks (`xw` or `xweek`)
* Month and the (x - 1) previous months (`xm` or `xmonth`)
* Year and the (x - 1) previous years (`xy` or `xyear`)

Show the total hours in a range (d/MM/yyyy) to (d/MM/yyyy):
* `r` or `range`

## File definition
You must provide a valid [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) file with Semicolons (`;`) as the separator.

## Columns limitations
### First column
The date of the day, with the format `d/MM/yyyy` ([1-31]/[01-12]/XXXX)

### Rest of the columns
The punches; represented as duplet of columns (can have as many you want).

The time is entered without any separator (`:`, etc.)

#### Limitations

##### Midnight
* `12:00 AM` : the correct value is `0`.
* `12:44 AM` : the correct value is `44`.

##### Hours < 10 AM
* Enter the time with `3` digits; `09:00 AM` will be `900`

## File example
```csv
16/11/2020;0;1200;1215;1700
17/11/2020;44;1200;1215;1700
18/11/2020;900;1200;1215;1700
```